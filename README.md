My custom DWM build.
============

# Dependencies
+ libxft
+ ttf-hack
+ ttf-joypixels
+ st
+ dmenu

# AUR Dependencies
+ nerd-fonts-complete (optional)
+ https://aur.archlinux.org/packages/libxft-bgra/ (needed for colored fonts and emojis)

# Installation
- make
- sudo make clean install
